# Automated testing with Gradle and JUnit

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/ceesparis/java-assignemnt1-rpg/badges/master/pipeline.svg)](https://gitlab.com/ceesparis/java-assignemnt1-rpg/-/commits/master)

This project is a console application in which you can create RPG characters and items. 
Various types of characters (Mage, Rogue, Ranger, Warrior) can be created, which all have the ability to level up and equip different kinds of weapons and armor.
As of yet, all the application logic is in place, but the user interaction through the console is not implemented yet. 


## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

No installation needed, all dependencies are downloaded through gradle.

## Usage

Open in Intellij and run. Requires Java 17.

## Maintainers

[@CeesParis](https://gitlab.com/ceesparis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Cees Paris
