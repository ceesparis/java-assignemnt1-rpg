package no.accelerate.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayableCharacterTest {

    @Test
    void create_newMage_shouldBeLvlOne(){
        Mage merlin = new Mage("Merlin");
        int expected = 1;
        int actual = merlin.getLevel();
        assertEquals(expected, actual);
    }
    @Test
    public void create_newWarrior_shouldHaveCorrectStats(){
        Warrior highlander = new Warrior("Higlander");
        int[] expected = {5, 2, 1};
        int[] actual = highlander.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }
    @Test
    public void create_newRogue_shouldHaveCorrectStats(){
        Rogue leon = new Rogue("Leon");
        int[] expected = {2, 6, 1};
        int[] actual = leon.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void create_newRanger_shouldHaveCorrectStats(){
        Ranger bowow = new Ranger("Bowow");
        int[] expected = {1, 7, 1};
        int[] actual = bowow.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void create_newMage_shouldHaveCorrectStats(){
        Mage merlin = new Mage("Merlin");
        int[] expected = {1, 1, 8};
        int[] actual = merlin.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void levelUp_warrior_shouldHaveCorrectStats(){
        Warrior highlander = new Warrior("Higlander");
        highlander.levelUp(highlander.levelGrowth);
        int[] expected = {8, 4, 2};
        int[] actual = highlander.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void levelUp_rogue_shouldHaveCorrectStats(){
        Rogue leon = new Rogue("Leon");
        leon.levelUp(leon.levelGrowth);
        int[] expected = {3, 10, 2};
        int[] actual = leon.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void levelUp_ranger_shouldHaveCorrectStats(){
        Ranger bowow = new Ranger("Bowow");
        bowow.levelUp(bowow.levelGrowth);
        int[] expected = {2, 12, 2};
        int[] actual = bowow.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void levelUp_mage_shouldHaveCorrectStats(){
        Mage merlin = new Mage("Merlin");
        merlin.levelUp(merlin.levelGrowth);
        int[] expected = {2, 2, 13};
        int[] actual = merlin.getPrimaryAttributes().getPoints();
        assertArrayEquals(expected, actual);
    }


}