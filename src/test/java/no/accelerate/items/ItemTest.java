package no.accelerate.items;

import no.accelerate.attributes.PrimaryAttributes;
import no.accelerate.characters.InvalidWeaponException;
import no.accelerate.characters.InvalidArmorException;
import no.accelerate.characters.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    public void equip_tooHighLevelWeapon_shouldThrowInvalidWeaponException() {
        Warrior doge = new Warrior("doge");
        Weapon axe = new Weapon("hacker", 3, 1.1, WeaponType.AXE, 2);
        Throwable exception = assertThrows(InvalidWeaponException.class, () -> doge.equipWeapon(axe));
        assertEquals("Your level is too low to equip this weapon", exception.getMessage());
    }

    @Test
    public void equip_tooHighLevelArmor_shouldThrowInvalidArmorException() {
        Warrior doge = new Warrior("doge");
        PrimaryAttributes helmetSpecs = new PrimaryAttributes(4, 1, 1);
        Armor helmet = new Armor("helmet", 2, SlotType.HEADSLOT, ArmorType.PLATE, helmetSpecs);
        Throwable exception = assertThrows(InvalidArmorException.class, () -> doge.equipArmor(helmet));
        assertEquals("Your level is too low to equip this armor", exception.getMessage());
    }

    @Test
    public void equip_wrongTypeWeapon_shouldThrowInvalidWeaponException(){
        Warrior doge = new Warrior("doge");
        Weapon bow = new Weapon("hacker", 3, 1.1, WeaponType.BOW, 1);
        Throwable exception = assertThrows(InvalidWeaponException.class, () -> doge.equipWeapon(bow));
        assertEquals("You cannot equip a weapon of type BOW", exception.getMessage());
    }

    @Test
    public void equip_wrongTypeArmor_shouldThrowInvalidArmorException() {
        Warrior doge = new Warrior("doge");
        PrimaryAttributes robeSpecs = new PrimaryAttributes(1, 1, 5);
        Armor robe = new Armor("robe", 2, SlotType.BODYSLOT, ArmorType.CLOTH, robeSpecs);
        Throwable exception = assertThrows(InvalidArmorException.class, () -> doge.equipArmor(robe));
        assertEquals("You cannot equip armor of type CLOTH", exception.getMessage());
    }

    @Test
    public void equip_suitableWeapon_shouldReturnTrue() throws InvalidWeaponException {
        Warrior doge = new Warrior("doge");
        Weapon axe = new Weapon("hacker", 3, 1.1, WeaponType.AXE, 1);
        assertTrue(doge.equipWeapon(axe));
    }

    @Test
    public void equip_suitableArmor_shouldReturnTrue() throws InvalidArmorException {
        Warrior doge = new Warrior("doge");
        PrimaryAttributes helmetSpecs = new PrimaryAttributes(4, 1, 1);
        Armor helmet = new Armor("helmet", 1, SlotType.HEADSLOT, ArmorType.PLATE, helmetSpecs);
        assertTrue(doge.equipArmor(helmet));
    }

    @Test
    public void totalDPS_lvl1EmptyHandedWarrior_shouldBeCorrectDPS(){
        Warrior doge = new Warrior("doge");
        double expected = 1 * (1+(5/100.0));
        double actual = doge.calculateCharacterDPS();
        assertEquals(expected, actual);
    }

    @Test
    public void totalDPS_lvl1AxeWarrior_shouldBeCorrectDPS() throws InvalidWeaponException {
        Warrior doge = new Warrior("doge");
        Weapon axe = new Weapon("hacker", 7, 1.1, WeaponType.AXE, 1);
        doge.equipWeapon(axe);
        double expected = (7 * 1.1) * (1+(5/100.0));
        double actual = doge.calculateCharacterDPS();
        assertEquals(expected, actual);
    }

    @Test
    public void totalDPS_lvl1AxeArmoredWarrior_shouldBeCorrectDPS() throws InvalidWeaponException, InvalidArmorException {
        Warrior doge = new Warrior("doge");
        Weapon axe = new Weapon("hacker", 7, 1.1, WeaponType.AXE, 1);
        doge.equipWeapon(axe);
        PrimaryAttributes helmetSpecs = new PrimaryAttributes(1, 0, 0);
        Armor helmet = new Armor("common plate body armor", 1, SlotType.BODYSLOT, ArmorType.PLATE, helmetSpecs);
        doge.equipArmor(helmet);
        double expected = (7 * 1.1) * (1+((5+1)/100.0));
        double actual = doge.calculateCharacterDPS();
        assertEquals(expected, actual);
    }

    @Test
    public void equip_newWeapon_shouldReplaceOldWeapon() throws InvalidWeaponException {
        Warrior doge = new Warrior("doge");
        Weapon axe = new Weapon("hacker", 7, 1.1, WeaponType.AXE, 1);
        doge.equipWeapon(axe);
        Weapon sword = new Weapon("sword", 7, 1.1, WeaponType.SWORD, 1);
        doge.equipWeapon(sword);
        Weapon actual = (Weapon) doge.getEquipment().get(SlotType.WEAPONSLOT);
        Weapon expected = sword;
        assertEquals(expected, actual);
    }


}