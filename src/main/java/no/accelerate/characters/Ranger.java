package no.accelerate.characters;

import no.accelerate.items.ArmorType;
import no.accelerate.items.WeaponType;

public class Ranger extends PlayableCharacter {
    public int[] levelGrowth = {1, 5, 1};
    private WeaponType[] possibleWeaponTypes = {WeaponType.BOW};
    private ArmorType[] possibleArmorTypes = {ArmorType.LEATHER, ArmorType.MAIL};
    private String mainPrimaryAttribute = "dexterity";

    public Ranger(String name) {
        super(name);
        setPrimaryAttributes(1, 7, 1);
        setPossibleWeaponTypes(possibleWeaponTypes);
        setPossibleArmorTypes(possibleArmorTypes);
        setMainPrimaryAttribute(mainPrimaryAttribute);
    }

}
