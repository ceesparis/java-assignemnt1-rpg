package no.accelerate.characters;

import no.accelerate.items.ArmorType;
import no.accelerate.items.WeaponType;

public class Rogue extends PlayableCharacter {
    public int[] levelGrowth = {1, 4, 1};
    private WeaponType[] possibleWeaponTypes = {WeaponType.SWORD, WeaponType.DAGGER};
    private ArmorType[] possibleArmorTypes = {ArmorType.LEATHER, ArmorType.MAIL};
    private String mainPrimaryAttribute = "dexterity";

    public Rogue(String name) {
        super(name);
        setPrimaryAttributes(2, 6, 1);
        setPossibleWeaponTypes(possibleWeaponTypes);
        setPossibleArmorTypes(possibleArmorTypes);
        setMainPrimaryAttribute(mainPrimaryAttribute);
    }
}
