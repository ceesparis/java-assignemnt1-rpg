package no.accelerate.characters;

public class DisplayCharacter {

    /**
     * Prints the specs of a character to the terminal window.
     *
     *
     * @param playableCharacter
     */
    public static void printPlayableCharacter(PlayableCharacter playableCharacter){
            playableCharacter.calculateTotalAttributes();
            double charDPS = playableCharacter.calculateCharacterDPS();
            int[] totalPoints = playableCharacter.getTotalPrimaryAttributes().getPoints();
            String stats = "Character name: " + playableCharacter.getName() +
                    "\nCharacter level: " + playableCharacter.getLevel() +
                    "\nStrength: " + totalPoints[0] +
                    "\nDexterity: " + totalPoints[1] +
                    "\nIntelligence: " + totalPoints[2] +
                    "\nDPS: " + charDPS;
            System.out.println(stats);
    }
}
