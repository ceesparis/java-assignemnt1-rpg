package no.accelerate.characters;

import no.accelerate.items.ArmorType;
import no.accelerate.items.WeaponType;

public class Warrior extends PlayableCharacter {
    public int[] levelGrowth = {3, 2, 1};
    private WeaponType[] possibleWeaponTypes = {WeaponType.SWORD, WeaponType.AXE, WeaponType.HAMMER};
    private ArmorType[] possibleArmorTypes = {ArmorType.PLATE, ArmorType.MAIL};
    private String mainPrimaryAttribute = "strength";

    public Warrior(String name) {
        super(name);
        setPrimaryAttributes(5, 2, 1);
        setPossibleWeaponTypes(possibleWeaponTypes);
        setPossibleArmorTypes(possibleArmorTypes);
        setMainPrimaryAttribute(mainPrimaryAttribute);
    }

}
