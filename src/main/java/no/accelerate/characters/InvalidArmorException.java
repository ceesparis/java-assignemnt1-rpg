package no.accelerate.characters;

public class InvalidArmorException extends Exception{
    InvalidArmorException(String message){
        super(message);
    }
}
