package no.accelerate.characters;

public class InvalidWeaponException extends Exception{
    InvalidWeaponException(String message){
        super(message);
    }
}
