package no.accelerate.characters;

import no.accelerate.attributes.PrimaryAttributes;
import no.accelerate.items.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public abstract class PlayableCharacter {

    private final String name;
    private Integer level = 1;
    private PrimaryAttributes primaryAttributes;
    private String mainPrimaryAttribute;
    private PrimaryAttributes totalPrimaryAttributes;
    private final HashMap<SlotType, Item> equipment = new HashMap<>();
    private ArrayList<WeaponType> possibleWeaponTypes = new ArrayList<>();
    private ArrayList<ArmorType> possibleArmorTypes = new ArrayList<>();

    public PlayableCharacter(String name) {
        this.name = name;
    }

    /**
     * Levels up a character, and increases primaryAttributes according to type character.
     *
     * @param typeLevelGrowth the increase in attributes specific to the character's type.
     */
    public void levelUp(int[] typeLevelGrowth){
        int[] currentValues = this.primaryAttributes.getPoints();
        ArrayList<Integer> newValues = new ArrayList<>();
        // for each attribute, add together the character's points and the
        // level growth for its type
        for(int i = 0; i<currentValues.length; i++){
            newValues.add(currentValues[i] + typeLevelGrowth[i]);
        }

        setPrimaryAttributes(newValues.get(0), newValues.get(1), newValues.get(2));
        this.level++;
    }

    /**
     * Tries to equip a weapon. If successful, returns true, else
     * throws InvalidWeaponException.
     *
     * @param weapon the Weapon object which the character wants to equip.
     * @return boolean true
     * @throws InvalidWeaponException a custom error for when the
     * character cannot equip this weapon.
     */
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        int requiredLevel = weapon.getReqLevel();
        if (!this.possibleWeaponTypes.contains(weapon.getWeaponType())) {
            throw new InvalidWeaponException("You cannot equip a weapon of type "+ weapon.getWeaponType());
        } else if (requiredLevel > this.level) {
            throw new InvalidWeaponException("Your level is too low to equip this weapon");
        }
        this.equipment.put(SlotType.WEAPONSLOT, weapon);

        return true;
    }

    /**
     * Tries to equip a piece of Armor. If successful, returns true, else
     * throws InvalidArmorException.
     *
     * @param armor the Armor object which the character is trying to equip.
     * @return boolean true
     * @throws InvalidArmorException a custom error for when the character
     * cannot equip this piece of armor.
     */
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        int requiredLevel = armor.getReqLevel();
        SlotType slotType = armor.getSlotType();
        if (!this.possibleArmorTypes.contains(armor.getArmorType())) {
            throw new InvalidArmorException("You cannot equip armor of type "+ armor.getArmorType());
        } else if (requiredLevel > this.level) {
            throw new InvalidArmorException("Your level is too low to equip this armor");
        } else {
            this.equipment.put(slotType, armor);
            return true;
        }
    }

    /**
     * Calculates and sets totalAttributes of character.
     *
     */
    public void calculateTotalAttributes(){
        int[] playerPoints = this.primaryAttributes.getPoints();
        ArrayList<Integer> totalPoints = new ArrayList<>(Arrays.asList(0, 0, 0));

        // for all the armor-items in equipment, add their attribute-points together in totalPoints.
        for(var entry : equipment.entrySet()) {
            if (entry.getValue() instanceof Armor){
                int[] armorPrimAtt = ((Armor) entry.getValue()).getPrimaryAttributes().getPoints();
                for (int i = 0; i < armorPrimAtt.length; i++){
                    totalPoints.set(i, totalPoints.get(i) + armorPrimAtt[i]);
                }
            }
        }

        // add characters attribute-points to totalPoints
        for (int i = 0; i < playerPoints.length; i++){
            totalPoints.set(i, totalPoints.get(i) + playerPoints[i]);
        }

        this.totalPrimaryAttributes =  new PrimaryAttributes(totalPoints.get(0), totalPoints.get(1), totalPoints.get(2));
    }

    /**
     * Calculates the DPS(damage per second) of currently equipped weapon.
     * If no weapon is equipped, weaponDPS defaults to 1.
     *
     *
     * @return double weaponDPS
     */
    public double calculateWeaponDPS(){
        Weapon currentWeapon = ((Weapon) equipment.get(SlotType.WEAPONSLOT));
        double dps = 1.0;
        if(currentWeapon != null){
            dps = currentWeapon.getDamage() * currentWeapon.getAttackSpeed();
        }
        return dps;
    }

    /**
     * Calculates the total DPS (damage per second) of character.
     *
     * @return double totalDPS
     */
    public double calculateCharacterDPS(){
        this.calculateTotalAttributes();
        double mainAttPoints = 0;
        String mainAtt = this.mainPrimaryAttribute;
        // check what the mainPrimaryAttribute of character is, and set accordingly
        switch (mainAtt) {
            case "strength" -> mainAttPoints = totalPrimaryAttributes.strength();
            case "dexterity" -> mainAttPoints = totalPrimaryAttributes.dexterity();
            case "intelligence" -> mainAttPoints = totalPrimaryAttributes.intelligence();
        }
        double weaponDPS = calculateWeaponDPS();
        return weaponDPS * (1+(mainAttPoints/100.0));
    }

    public void setPossibleWeaponTypes(WeaponType[] possibleWeaponTypes) {
        this.possibleWeaponTypes = new ArrayList<>(Arrays.asList(possibleWeaponTypes));
    }

    public void setPossibleArmorTypes(ArmorType[] possibleArmorTypes) {
        this.possibleArmorTypes = new ArrayList<>(Arrays.asList(possibleArmorTypes));
    }

    public void setMainPrimaryAttribute(String mainPrimaryAttribute) {
        this.mainPrimaryAttribute = mainPrimaryAttribute;
    }

    public String getName() {
        return name;
    }

    public Integer getLevel() {
        return level;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.primaryAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
    }
    public HashMap<SlotType, Item> getEquipment(){
        return this.equipment;
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

}
