package no.accelerate.characters;

import no.accelerate.items.ArmorType;
import no.accelerate.items.WeaponType;

public class Mage extends PlayableCharacter {
    public int[] levelGrowth = {1, 1, 5};
    private WeaponType[] possibleWeaponTypes = {WeaponType.STAFF, WeaponType.WAND};
    private ArmorType[] possibleArmorTypes = {ArmorType.CLOTH};
    private String mainPrimaryAttribute = "intelligence";

    public Mage(String name) {
        super(name);
        setPrimaryAttributes(1, 1, 8);
        setPossibleWeaponTypes(possibleWeaponTypes);
        setPossibleArmorTypes(possibleArmorTypes);
        setMainPrimaryAttribute(mainPrimaryAttribute);
    }
}
