package no.accelerate.items;

public abstract class Item {
    private String name;
    private int reqLevel;
    private SlotType slotType;


    public Item(String name, int reqLevel, SlotType slotType) {
        this.name = name;
        this.reqLevel = reqLevel;
        this.slotType = slotType;
    }

    public Item(String name) {
    }

    public int getReqLevel() {
        return reqLevel;
    }

    public void setReqLevel(int reqLevel) {
        this.reqLevel = reqLevel;
    }

    public SlotType getSlotType() {
        return slotType;
    }

    public void setSlotType(SlotType slotType) {
        this.slotType = slotType;
    }


}
