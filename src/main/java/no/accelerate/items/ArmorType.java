package no.accelerate.items;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE;
}
