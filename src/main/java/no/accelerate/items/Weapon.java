package no.accelerate.items;


public class Weapon extends Item{

    SlotType slotType = SlotType.WEAPONSLOT;
    WeaponType weaponType;
    private double damage;
    private double attackSpeed;


    public Weapon(String name, double damage, double attackSpeed, WeaponType weaponType, int reqLevel) {
        super(name);
        this.setSlotType(slotType);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
        this.setReqLevel(reqLevel);
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public double getDamage() {
        return damage;
    }


    public double getAttackSpeed() {
        return attackSpeed;
    }

}
