package no.accelerate.items;

public enum SlotType {
    WEAPONSLOT,
    HEADSLOT,
    BODYSLOT,
    LEGSSLOT;

}
