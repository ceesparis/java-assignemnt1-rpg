package no.accelerate.items;

import no.accelerate.attributes.PrimaryAttributes;

public class Armor extends Item{

    ArmorType armorType;

    PrimaryAttributes primaryAttributes;

    public ArmorType getArmorType() {
        return armorType;
    }

    public Armor(String name, int reqLevel, SlotType slotType, ArmorType armorType, PrimaryAttributes primaryAttributes) {
        super(name, reqLevel, slotType);
        this.armorType = armorType;
        this.primaryAttributes = primaryAttributes;
    }
    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }
}
