package no.accelerate.attributes;

public record PrimaryAttributes(int strength, int dexterity, int intelligence) {

    /**
     *
     * Retrieves all the values stored in a
     * PrimaryAttributes record object at once.
     *
     * @return an int array containing the current values of the primary attributes.
     */
    public int[] getPoints() {
        int[] points = {this.strength, this.dexterity, this.intelligence};
        return points;
    }

}
